//
//  GitRevertApp.swift
//  GitRevert
//
//  Created by Zin Lin Htet Naing on 16/05/2023.
//

import SwiftUI

@main
struct GitRevertApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
